build:
		git branch -D release || true
		git prune
		yarn build:production
		git checkout --orphan release
		grep -v dist .gitignore > _gitignore
		mv _gitignore .gitignore
		git add -A
		git commit -v -a -m 'Automatic commit'
		git push origin -f release
		git checkout master

{{--
  Template Name: Catégories récompensées
--}}

@extends('layouts.full')

@section('content')
    @while(have_posts()) @php the_post() @endphp
      <div class="pages pages--awards-categories">
        @include('partials.page-header')
        @include('partials.content-page')
        <nav class="pages--awards-categories__menu mt-5">
          <div>
            <h4>Sommaire</h4>
            <ol>
              @foreach (get_terms(array('taxonomy' => 'awards_category', 'hide_empty' => true)) as $category)
                <li>
                  <a href="#c{{ $category->term_id }}">
                    {{ $category->name }}
                  </a>
                </li>
              @endforeach
            </ol>
          </div>
        </nav>
        <ul class="pages--awards-categories__list list-unstyled">
          @foreach (get_terms(array('taxonomy' => 'awards_category', 'hide_empty' => true)) as $category)
            <li class="pages--awards-categories__list__item" id="c{{ $category->term_id }}">
              <h4 class="pages--awards-categories__list__item__heading pb-5">
                Dans la catégorie «&nbsp;{{ $category->name }}&nbsp;»
              </h4>
              <div class="pages--awards-categories__list__item__description">
                {{ $category->description }}
              </div>
              <!--h5 class="pages--awards-categories__list__item__inter">
                Les nominé•e•s
              </h5-->
              <div class="pages--awards-categories__list__item__nominees mt-4">
                <div class="container-fluid">
                  <div class="row">
                    @foreach (App::get_nominees_by_category($category->term_id) as $nominee)
                      <div class="col-12 pages--awards-categories__list__item__nominees__item row no-gutters">
                        <div class="col">
                          <div class="pages--awards-categories__list__item__nominees__item__thumbnail">
                            {!! get_the_post_thumbnail($nominee, 'medium') !!}
                          </div>
                        </div>
                        <div class="col-8 pl-5">
                          <div class="pages--awards-categories__list__item__nominees__item__name">
                            {{ $nominee->post_title }}
                          </div>
                          <div class="pages--awards-categories__list__item__nominees__item__description">
                            {!! $nominee->post_content !!}
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </li>
          @endforeach
        </ul>
      </div>
    @endwhile
@endsection

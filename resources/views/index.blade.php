@extends('layouts.full')

@section('content')
  <div class="index">
    <div class="index__introduction container">
      <div class="row">
        <div class="col-3 d-sm-none d-none d-md-block ">
          <img src="@asset('images/logo-outdor.svg')" alt="Out d'Or 2018" class="w-100" />
        </div>
        <div class="col pt-4">
          @php dynamic_sidebar('sidebar-introduction') @endphp
        </div>
      </div>
    </div>

    <div class="index__schedule">
      <div class="container">
        <div class="row">
          <div class="col offset-md-3 index__schedule__widgets">
            @php dynamic_sidebar('sidebar-schedule') @endphp
          </div>
        </div>
      </div>
    </div>

    <div class="index__nominees bg-tertiary text-light m-3 mb-0">
      <div class="container">
        @php dynamic_sidebar('sidebar-nominees') @endphp
        <a href="{{ get_page_link(get_page_by_title('Les nommé·e·s')->ID) }}" class="btn btn-lg btn-light mt-4">
          Découvrir les nommé•e•s
        </a>
      </div>
    </div>

    <div class="index__teaser">
      <div class="index__teaser__wrapper">
        <iframe class="index__teaser__wrapper__video" width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/07L1VDwM6k4?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>

    <!--div class="index__location bg-muted m-3 mb-0" id="index__location"></div-->

    <div class="index__links text-light">
      <div class="container-fluid text-center">
        <div class="row">
          <section class="col-lg-6 col-md-12 index__links__section index__links__section--last-edition bg-dark">
            <p class="index__links__section__lead">
              La toute première édition des OUT d'or s'est tenue le 29 juin 2017.
            </p>
            <a href="{{ get_page_link(get_page_by_path('edition 2017')->ID) }}" class="btn btn-lg btn-light">
              L'édition 2017
            </a>
          </section>
          <section class="col-lg-6 col-md-12 index__links__section index__links__section--external-event bg-dark">
            <p class="index__links__section__lead">
              Le débat "fake news et discours de haine des minorités" est ouvert à tou·te·s, entrée libre et gratuite, inscription sur le site de la Maison des Métallos.
            </p>
            <a target="_blank" href="http://www.maisondesmetallos.paris/2018/02/26/les-out-d-or" class="btn btn-lg btn-light">
              Inscrivez-vous
            </a>
          </section>
        </div>
      </div>
    </div>
    <div class="index__sponsors">
      <div class="container">
        @php dynamic_sidebar('sidebar-sponsors-soutiens') @endphp
      </div>
    </div>
  </div>
@endsection

<header class="header">
  <div class="navbar navbar-expand-lg navbar-light">
    <div class="col px-0">
      @unless (is_home())
        <a class="navbar-brand" href="{{ home_url('/') }}">
          <img src="@asset('images/logo-outdor-horizontal.svg')" alt="Out d'Or 2018" />
        </a>
      @else
        <a class="navbar-brand d-xs-block d-md-none" href="{{ home_url('/') }}">
          <img src="@asset('images/logo-outdor-horizontal.svg')" alt="Out d'Or 2018" />
        </a>
      @endunless
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-navigation" aria-controls="primary-navigation" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="primary-navigation">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'header__nav']) !!}
      @endif
      @if (has_nav_menu('programme'))
        <ul class="header__nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Le programme
            </a>
            {!! wp_nav_menu(['theme_location' => 'programme', 'menu_class' => 'dropdown-menu dropdown-menu-right']) !!}
          </li>
        </ul>
      @endif
    </div>
  </div>
</header>

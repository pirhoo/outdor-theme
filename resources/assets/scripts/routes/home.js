import L from 'leaflet';


export default {
  // Center of the map
  center: [48.8677270, 2.3778980],
  popup: `<div class="row no-gutters">
    <div class="col-5 p-4 leaflet-popup__logo">
      <img src="${require('../../images/logo-maison-des-metallos.svg')}" class="w-100" />
    </div>
    <div class="col-7 leaflet-popup__addr bg-primary text-white">
      <p class="mt-0">
        <strong>94 rue Jean-Pierre Timbaud, Paris 11e</strong>
      <p>
      <p>
        <img src="${require('../../images/metro.svg')}" alt="métro" /> Métro ligne 2 arrêt Couronnes<br />
        <img src="${require('../../images/metro.svg')}" alt="métro" /> Métro ligne 3 arrêt Parmentier
      </p>
      <p class="mb-0">
        <img src="${require('../../images/bus.svg')}" alt="bus" /> Bus ligne 96, arrêt Maison des métallos (direction Gare Montparnasse) ou
        arrêt Saint-Maur/Jean Aicard (direction Porte des Lilas).
      </p>
    </div>
  </div>`,
  finalize() {
    // this.buildMap();
  },
  buildMap() {
    // Create map
    this.map = L.map('index__location', {
      center: this.center,
      zoom: 16,
      scrollWheelZoom: false,
      closePopupOnClick: false,
    });
    // Add positron base tile
    L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: 'Positron',
    }).addTo(this.map);
    // Add a mark
    L.popup({
      closeButton: false,
      maxWidth: 660,
    }).setLatLng(this.center).setContent(this.popup).openOn(this.map);
  },
};

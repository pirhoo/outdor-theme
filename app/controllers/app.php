<?php

namespace App;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function get_nominees_by_category($term_id)
    {
      return get_posts(
        array(
          'numberposts' => -1,
          'post_type' => 'nominee',
          'tax_query' => array(
            array(
              'taxonomy' => 'awards_category',
              'field' => 'id',
              'terms' => $term_id,
              'include_children' => false
            )
          )
        )
      );
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}

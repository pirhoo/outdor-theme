<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme post types
 */
add_action('init', function () {
  $nominee = array(
    'label'              => __( 'Nommé•e•s', 'sage' ),
    'description'        => __( 'Un•e nommé•e dans une catégorie', 'sage' ),
    'public'             => true,
    'publicly_queryable' => false,
    'exclude_from_search'=> true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 10,
    'menu_icon'          => 'dashicons-awards',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail')
  );

  $awards_category = array(
    'label'        => __( 'Catégories', 'sage' ),
    'public'       => true,
    'hierarchical' => false
  );

 	register_post_type('nominee', $nominee);
  register_taxonomy('awards_category', 'nominee', $awards_category);
  register_taxonomy_for_object_type('awards_category', 'nominee');
});


/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
}, 100);

add_filter('upload_mimes', function ($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
});

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'programme' => __('Programme', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget__heading">',
        'after_title'   => '</h3>'
    ];

    register_sidebar([
        'name'          => __('Introduction', 'sage'),
        'id'            => 'sidebar-introduction',
        'before_widget' => '<section class="widget widget--introduction %1$s %2$s">'
    ] + $config);

    register_sidebar([
        'name'          => __('Le programme de l\'évènement', 'sage'),
        'id'            => 'sidebar-schedule',
        'before_title'  => '<h4 class="widget__heading">',
        'after_title'   => '</h4>',
        'before_widget' => '<section class="widget widget--schedule %1$s %2$s">'
    ] + $config);

    register_sidebar([
        'name'          => __('Les organisateurs', 'sage'),
        'id'            => 'sidebar-organizers',
        'before_widget' => '<section class="widget widget--organizers %1$s %2$s">'
    ] + $config);

    register_sidebar([
        'name'          => __('Les nominé•e•s', 'sage'),
        'id'            => 'sidebar-nominees',
        'before_widget' => '<section class="widget widget--nominees %1$s %2$s">'
    ] + $config);

    register_sidebar([
        'name'          => __('Les sponsors et soutiens', 'sage'),
        'id'            => 'sidebar-sponsors-soutiens',
        'before_widget' => '<section class="widget widget--sponsors %1$s %2$s">'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});
